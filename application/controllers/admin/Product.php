<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('common_model');
		$aLoggedInUser = array();
        $aLoggedInUser = $this->session->userdata('active_user');
        if(empty($aLoggedInUser)){
            redirect('admin/Auth');
        }
	}
	public function index(){
        $aData['data'] = $this->db->get_where('product','deleted_at is null')->result_array();
        $this->load->template_admin_general('admin/product_list',$aData);
	}
    public function add(){       
        $data['category'] = $this->db->get_where('category','deleted_at is null')->result();
        $this->load->template_admin_general('admin/add_new_product',$data);
    }
    public function edit(){
        $data['category'] = $this->db->get_where('category','deleted_at is null')->result();
        $data['data'] = $this->db->get_where('product',['id'=> $this->uri->segment(4)])->result();
        $this->load->template_admin_general('admin/add_new_product',$data);
    }
    public function delete(){  
        if($this->db->update('product',['deleted_at'=>date("Y-m-d H:i:s")],['id'=>$this->uri->segment(4)])){
            $this->session->set_flashdata('message_success', 'Data Successfully Deleted.');
        }else{
            $this->session->set_flashdata('message_danger', 'Data Not Deleted.');
        }    
        redirect('admin/Product');
    }

    public function delete_img(){  
        $data = $this->db->get_where('product',['id'=> $this->uri->segment(4)])->result();
        $images = json_decode($data[0]->images,true);
        unset($images[array_search($this->uri->segment(5),$images)]);
        unlink('./assets/uploads/product/'.$this->uri->segment(5));
        $images = json_encode($images);
        if($this->db->update('product',['images'=>$images],['id'=>$this->uri->segment(4)])){
            $this->session->set_flashdata('message_success', 'Data Successfully Deleted.');
        }else{
            $this->session->set_flashdata('message_danger', 'Data Not Deleted.');
        }    
        redirect('admin/Product/edit/'.$this->uri->segment(4));
    }

    public function save(){
        $propdata = $this->input->post();  
        $propdata['images'] = [];     
        if(!empty($_FILES['userFiles']['name'])){
             $filesCount = count($_FILES['userFiles']['name']);
             for($i = 0; $i < $filesCount; $i++){
                $_FILES['userFile']['name'] = $_FILES['userFiles']['name'][$i];
                $_FILES['userFile']['type'] = $_FILES['userFiles']['type'][$i];
                $_FILES['userFile']['tmp_name'] = $_FILES['userFiles']['tmp_name'][$i];
                $_FILES['userFile']['error'] = $_FILES['userFiles']['error'][$i];
                $_FILES['userFile']['size'] = $_FILES['userFiles']['size'][$i];
                
                $config['upload_path'] = './assets/uploads/product/';
                 $config['allowed_types'] = 'gif|jpg|png';
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if($this->upload->do_upload('userFile')){                 
                    $fileData = $this->upload->data();
                    $propdata['images'][$i] = $fileData['file_name'];                    
                }
            }
        }
        if(!empty($propdata['images'])){
            $propdata['images'] = json_encode($propdata['images']);    
        }else {
         unset($propdata['images']);
        }
            $result = 0;
            $optration = "";
            if($propdata['id'] > 0){
                $result = $this->db->update('product', $propdata,['id'=>$propdata['id']]);
                $optration = "Updatede";
            }else{
                $result = $this->db->insert('product', $propdata);
                $optration = "inserted";
            }
            if($result){
                $this->session->set_flashdata('message_success', 'Data Successfully '.$optration.'.');
            }else{
                $this->session->set_flashdata('message_danger', 'Data Not '.$optration.'.');
            }        
        redirect('admin/Product');
    }
}	