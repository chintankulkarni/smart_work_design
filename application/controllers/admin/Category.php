<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('common_model');
		$aLoggedInUser = array();
        $aLoggedInUser = $this->session->userdata('active_user');
        if(empty($aLoggedInUser)){
            redirect('admin/Auth');
        }
	}
	public function index(){        
        $aData['data'] = $this->db->get_where('category','deleted_at is null')->result_array();
        $this->load->template_admin_general('admin/category_list',$aData);
	}
    public function add(){       
        $this->load->template_admin_general('admin/add_new_category');
    }
    public function edit(){    
        $data['data'] = $this->db->get_where('category',['id'=> $this->uri->segment(4)])->result();
        $this->load->template_admin_general('admin/add_new_category',$data);
    }
    public function delete(){  
        if($this->db->update('category',['deleted_at'=>date("Y-m-d H:i:s")],['id'=>$this->uri->segment(4)])){
            $this->session->set_flashdata('message_success', 'Data Successfully Deleted.');
        }else{
            $this->session->set_flashdata('message_danger', 'Data Not Deleted.');
        }    
        redirect('admin/Category');   
    }
    public function save(){
        $propdata = $this->input->post();
            $result = 0;
            $optration = "";
            if($propdata['id'] > 0){
                $result = $this->db->update('category', $propdata,['id'=>$propdata['id']]);
                $optration = "Updatede";
            }else{
                $result = $this->db->insert('category', $propdata);
                $optration = "inserted";
            }
            
            if($result){
                $this->session->set_flashdata('message_success', 'Data Successfully '.$optration.'.');
            }else{
                $this->session->set_flashdata('message_danger', 'Data Not '.$optration.'.');
            }        
        redirect('admin/Category');        
    }
}	
