<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	private $sTable1 = "users";

	public function __construct(){
		parent::__construct ();
		$this->load->model ( 'common_model' );
	}
	public function index(){
		$this->load->template_admin('admin/login');
	}
	public function post_login(){
		if ( $this->input->post( 'email' ) AND $this->input->post( 'password' ) ) {
			$aWhere[ 'email' ] = $this->input->post( 'email' );
			$aWhere[ 'password' ] = $this->input->post( 'password' );
			if ( $oRow = $this->common_model->get_row( $this->sTable1 , $aWhere ) ) {
				$this->session->set_userdata( 'active_user' , get_object_vars( $oRow ) );
				redirect( 'admin/Dashboard' );
			}
			$this->session->set_userdata( 'error-login-fail' , 'Invalid Credentials' );
			redirect( 'admin/Auth' );
		}
		$this->session->set_userdata( 'error-empty-fields' , 'Email and Password are required' );
		redirect( '/' );
	}
	public function logout(){
		$this->session->unset_userdata('active_user');
		redirect('admin/Auth');
	}
	public function forgot_password(){
		$this->load->template_admin('admin/forgot_password');
	}
	public function post_forgot_password(){
		if( $this->input->post('email') ){
			$aWhere['email'] = $this->input->post('email');
			if ( $oRow = $this->common_model->get_row( $this->sTable1 , $aWhere ) ) {
				// Send email
				$from_email = "chintank.tps@gmail.com"; 
		        $to_email = $oRow->email; 
		        $message = "Hello Admin,<br> Your email address and password are as following,<br>Email : ".$oRow->email."<br>Password : ".$oRow->password." Thank you.";
		   		$this->email->from($from_email, 'Instant Green Loan'); 
		        $this->email->to($to_email);
		        $this->email->subject('Forgot Password'); 
		        $this->email->message($message); 
		   		$this->email->send(); 
		   		$this->email->print_debugger();	
		   		$this->session->set_userdata( 'msg-forgot-password' , 'Password has been sent at your email address.' );		
		   		redirect( 'admin/Auth' );
			}
		}
	}
	public function change_password(){
		$aData['aCurrentUserInfo'] = $this->session->userdata('active_user');
		$this->load->template_admin_template('admin/change_password',$aData);	
	}
	public function post_change_password(){
		if( $this->input->post('uid') && $this->input->post('password') ){
			$aWhere['id'] = $this->input->post('uid');
			$aData['password'] = $this->input->post('password');
			$this->db_common->update( $this->sTable1 , $aData , $aWhere );
			redirect('admin/Dashboard/');
		}
	}
}