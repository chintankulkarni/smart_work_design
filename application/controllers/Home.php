<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct(){
		parent::__construct ();
		$this->load->model('common_model', 'db_common' );
		$this->load->library('email'); 
	}
	public function index(){		
		redirect(base_url('/admin'),'refresh');
	}
}