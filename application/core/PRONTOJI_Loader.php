<?php

class PRONTOJI_Loader extends CI_Loader{
    
    public function __construct(){
        parent::__construct();
    }

    public function template_admin( $sTemplateName = "" , $aVar = array() ,$bReturn = false ){
        $sContent = $this->view( 'admin/header' , $aVar , $bReturn );
        $sContent = $this->view( $sTemplateName , $aVar , $bReturn );
        $sContent = $this->view( 'admin/footer' , $aVar , $bReturn );
        return $sContent;
    }

    public function template_admin_general( $sTemplateName = "" , $aVar = array() ,$bReturn = false ){
        $sContent = $this->view( 'admin/header_template' , $aVar , $bReturn );
        $sContent = $this->view( $sTemplateName , $aVar , $bReturn );
        $sContent = $this->view( 'admin/footer_template' , $aVar , $bReturn );
        return $sContent;
    }

    // public function template_front($sTemplateName = "" , $aVar = array() , $bReturn = false ){
    //     $sContent = $this->view( 'front/header' , $aVar , $bReturn );
    //     $sContent = $this->view($sTemplateName , $aVar , $bReturn );
    //     $sContent = $this->view( 'front/footer' , $aVar , $bReturn );
    //     return $sContent;
    // }

    //  public function template_user($sTemplateName = "" , $aVar = array() , $bReturn = false ){
    //     $sContent = $this->view( 'user/header' , $aVar , $bReturn );
    //     $sContent = $this->view($sTemplateName , $aVar , $bReturn );
    //     $sContent = $this->view( 'user/footer' , $aVar , $bReturn );
    //     return $sContent;
    // }
}