<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Smart Work Design :: Admin</title>
    <link href="<?php echo base_url(); ?>assets/admin/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/admin/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/admin/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/admin/css/default.css" rel="stylesheet">    
    <link href="<?php echo base_url(); ?>assets/admin/css/demo-form.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/admin/css/color_theme.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/admin/css/view_reports.css" rel="stylesheet">       
    <link href="<?php echo base_url(); ?>assets/admin/css/sweetalert2.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/admin/datatables/css/dataTables.bootstrap.css" rel="stylesheet">
   
    <link href="<?php echo base_url(); ?>assets/admin/css/blue.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/admin/css/dev_css.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/admin/css/chosen.min.css" rel="stylesheet">
    <script src="<?php echo base_url(); ?>assets/admin/js/jquery.js" type="text/javascript" ></script>
    <script src="<?php echo base_url(); ?>assets/admin/js/jquery.validate.js" type="text/javascript" ></script>
    <script src="<?php echo base_url(); ?>assets/admin/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/admin/js/sweetalert2.min.js" type="text/javascript" ></script>
    <script src="<?php echo base_url(); ?>assets/admin/datatables/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/datatables/js/dataTables.bootstrap.js"></script>
    
    <script src="<?php echo base_url(); ?>assets/admin/js/retina.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/admin/js/function.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/admin/js/icheck.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/admin/js/script.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/admin/js/chosen.jquery.min.js" type="text/javascript" ></script>
    <script src="<?php echo base_url(); ?>assets/admin/js/chosen.proto.min.js" type="text/javascript" ></script>
    <script>
        var base_url  = '<?php echo base_url(); ?>';
    </script>
</head>
<body>
<div class="page-wrapper">
    <div id="wrapper" class="toggled">
        <!-- Sidebar Start-->
        <ul class="sidebar_wrap navbar navbar-inverse navbar-fixed-top col-xs-12" id="sidebar-wrapper" role="navigation">
           <li class="col-xs-12 {{ (Request::is('/') ? 'active' : '') }}">
                <a href="<?php echo base_url(); ?>admin/category" class="main-link {{ (Request::is('/') ? 'active_nav' : '') }}" title="Category">
                    <span><i class="glyphicon glyphicon-tasks"></i><span>Category</span></span>
                </a>
           </li>         
           <li class="col-xs-12 {{ (Request::is('/') ? 'active' : '') }}">
                <a href="<?php echo base_url(); ?>admin/Product" class="main-link {{ (Request::is('/') ? 'active_nav' : '') }}" title="Product">
                    <span><i class="glyphicon glyphicon-tasks"></i><span>Product</span></span>
                </a>
           </li>  
        </ul>
        <!-- Sidebar End-->
        <!-- Main page content including header-->
        <div id="page-content-wrapper">
            <!-- Header start -->
            <div class="header_wrap navbar-fixed-top">
            <div class="menu_icon" class="hamburger is-closed" id="leftbtn">
                <i class="fa fa-bars"></i>
            </div>
            <div class="logo">
                <!-- <a href="javascript:;"><img src="#" alt=""></a> -->
            </div>
            <div class="my_account">
                <div class="btn-group account_wrap">
                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        <span class=""><i class="fa fa-user"></i></span> <span class="u_name hidden-xs"></span> Admin <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu account_dropdown" role="menu">
                        <?php /*<li><a href="<?php echo base_url(); ?>admin/Auth/change_password"><span><i class="fa fa-user"></i></span> Change Password</a></li>*/ ?>
                        <li><a href="<?php echo base_url(); ?>admin/Auth/logout"><span><i class="fa fa-power-off"></i></span> Logout </a></li>
                    </ul>
                </div>
            </div>
            <a class="icons-toggle-btn" title="More options"><i class="glyphicon glyphicon-chevron-down"></i></a>
            <div class="head-select" id="school-list-div"></div>
            </div>
<!-- Header End--> 
	