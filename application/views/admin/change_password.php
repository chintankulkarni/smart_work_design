<div class="row">
    <div class="col-xs-12 col-sm-12">
        <form action="<?php echo base_url(); ?>admin/Auth/post_change_password" autocomplete="on" id="change-password" method="POST">
            <div class="panel panel-default">
                <ul class="list-group">
                    <li class="list-group-item border-none">
                        <div class="row custom-form-row">
                            <div class="col-sm-4">
                                <p class="feature_title form-label-title">
                                    Current Password <span class="asterix"> *</span>
                                </p>
                            </div>
                            <input type="hidden" name="uid" value="<?php echo $aCurrentUserInfo['id']; ?>" id="uid">
                            <div class="col-sm-8">
                                <div class="form-group">
                                	<input type="hidden" value="" name="id">
                                    <input type="password" class="form-control custom-form-control" name="pwd" id="pwd" placeholder="Current Password" value="">
                                    <span class="help-block incorrect-password"></span>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="list-group-item border-none">
                        <div class="row custom-form-row">
                            <div class="col-sm-4">
                                <p class="feature_title form-label-title">
                                     New Password <span class="asterix"> *</span>
                                </p>
                            </div>
                            <div class="col-sm-8">
                                <div class="form-group">
                                    <input type="password" class="form-control custom-form-control" name="password" id="password" placeholder="New Password" value="">
                                </div>
                                
                            </div>
                        </div>
                    </li>
                    <li class="list-group-item border-none">
                        <div class="row custom-form-row">
                            <div class="col-sm-4">
                                <p class="feature_title form-label-title">
                                     Confirm Password <span class="asterix"> *</span>
                                </p>
                            </div>
                            <div class="col-sm-8">
                                <div class="form-group">
                                    <input type="password" class="form-control custom-form-control" name="inputConPwd" id="inputConPwd" placeholder="Confirm Password" value="">
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="col-sm-12 text-center">
                    <input type="submit" class="btn custom-btn custom_btn  custom-save-btn" value="Save">
                </div>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
$(function(){
    $('#pwd').blur(function(){
        var inputOldPassword = $('#pwd').val();
        var uid = $("#uid").val();
        var sCurrentUserPassword = '<?php echo $aCurrentUserInfo['password']; ?>'
            //console.log( inputOldPassword + sCurrentUserPassword);return false;
        if( inputOldPassword != ""){
            if( inputOldPassword != sCurrentUserPassword ){
                $(".incorrect-password").html("Password is Incorrect!");
                //$("#pwd").focus();
            }else{ 
                $(".incorrect-password").hide();
            }
        }
    });
});
</script>

<script type="text/javascript">
$(function(){
    $("#change-password").validate({
        errorClass:'help-block',
        errorElement:'span',

        rules:{
            pwd:{required:true},
            password:{required:true, minlength: 6},
            inputConPwd:{required:true, equalTo : "#password" }
        },
        messages: {
            "pwd": {
                required: "Please Enter Current Password"
            },
            "password": {
                required: "Please Enter New Password",
                minlength: "Minimum 6 charactor is required"
            },
            "inputConPwd": {
                required: "Please Confirm Password"
            }
        },
        highlight: function (element, errorClass, validClass) 
        { 
            $(element).parents("div.form-group")
                      .addClass(errorClass); 
        },  
    });
});
</script>
