<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Smart Work Design :: Admin</title>
    <link href="<?php echo base_url(); ?>assets/admin/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/admin/css/style.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/admin/css/color_theme.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/admin/css/login.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>assets/admin/css/dev_css.css" rel="stylesheet">
    
	<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/js/jquery.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/js/jquery.validate.js"></script>
    <script src="<?php echo base_url(); ?>assets/admin/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>assets/admin/js/retina.min.js" type="text/javascript"></script>
    <script>
        var base_url = '<?php echo base_url(); ?>';
    </script>
</head>
<body class="login_body">