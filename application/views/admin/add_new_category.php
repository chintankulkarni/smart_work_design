<div class="row">
    <div class=" col-xs-12 col-sm-12">
        <div class="page_title view-page-title">
            <h2 class="mt-10"> <?php echo isset($data)?'Update':'Add New'; ?> Category</h2>
        </div>
    </div>
</div>

<!-- Basic details -->
<div class="row">
    <div class="col-xs-12 col-sm-12">        
        <form action="<?php echo base_url('admin/Category/save');?>" id="add_amenity" name="add_amenity" method="post"  enctype="multipart/form-data">
            <div class="panel panel-default">
                <ul class="list-group">
                    <li class="list-group-item border-none">
                        <div class="row custom-form-row">
                            <div class="col-sm-3">
                                <p class="feature_title form-label-title">
                                    Name
                                </p>
                            </div>
                            <div class="col-sm-9">
                                <div class="form-group">
                                    <input type="hidden" name="id" value="<?php echo isset($data)? ( $data[0]->id)?$data[0]->id:'0':'0'; ?>">                                  
                                    <input type="text" class="form-control custom-form-control" name="name" id="name" value="<?php echo isset($data)?$data[0]->name:''; ?>" placeholder="Title" required>
                                </div>
                            </div>
                        </div>
                    </li>                 
                </ul>

            </div>
            <div class="row">
                <div class="col-sm-12 text-center">
                    <button type="submit" class="btn custom-btn custom_btn  custom-save-btn">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript">
    $('form[name="add_amenity"]').validate({
       rules: {
              'name':{required: true}
        },
        errorPlacement: function(error, element){
            error.insertAfter(element);
        },
    });
</script>