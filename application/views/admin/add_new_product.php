<div class="row">
    <div class=" col-xs-12 col-sm-12">
        <div class="page_title view-page-title">
            <h2 class="mt-10"> <?php echo isset($data)?'Update':'Add New'; ?> Product</h2>
        </div>
    </div>
</div>

<!-- Basic details -->
<div class="row">
    <div class="col-xs-12 col-sm-12">        
        <form action="<?php echo base_url('admin/Product/save');?>" id="add_product" name="add_product" method="post"  enctype="multipart/form-data">
            <div class="panel panel-default">
                 <?php 
            if(($this->session->flashdata('message_success'))){
                ?>
                <div class="alert alert-success fade in alert-dismissible" style="margin-top:18px;">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                    <strong>Success!</strong> <?php echo $this->session->flashdata('message_success'); ?>
                </div>
                <?php                
            }
            if(($this->session->flashdata('message_danger'))){
                ?>
                <div class="alert alert-danger  fade in alert-dismissible" style="margin-top:18px;">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                    <strong>Danger!</strong> <?php echo $this->session->flashdata('message_danger'); ?>
                </div>
                <?php                
            }
        ?>
                <?php if(isset($data) && !empty($data[0]->images)){?>                
                <ul class="list-group">                    
                    <li class="list-group-item border-none">
                        <h2>Images</h2>
                        <div class="row custom-form-row">
                        <?php foreach (json_decode($data[0]->images) as $img) { ?>
                            <div class="col-sm-2">
                                <img class="thumbnail img-thumbnail img-responsive" src="<?php echo base_url('assets/uploads/product/').$img;?>" style="height:100px;width:100px;">
                                <a href="<?php echo base_url('admin/Product/delete_img/'.$data[0]->id.'/'.$img);?>"><button type="button" class="btn btn-sm btn-danger">Delete</button></a>
                            </div>
                        <?php } ?>
                        </div>
                </ul>
                <?php }?>
                <ul class="list-group">
                    <li class="list-group-item border-none">
                        <div class="row custom-form-row">
                            <div class="col-sm-3">
                                <p class="feature_title form-label-title">
                                    Title
                                </p>
                            </div>
                            <div class="col-sm-9">
                                <div class="form-group">
                                    <input type="hidden" name="id" value="<?php echo isset($data)? ( $data[0]->id)?$data[0]->id:'0':'0'; ?>">                                  
                                    <input type="text" class="form-control custom-form-control" name="name" id="name" value="<?php echo isset($data)?$data[0]->name:''; ?>" placeholder="Title" required>
                                </div>
                            </div>
                        </div>
                    </li>



                    <li class="list-group-item border-none">
                        <div class="row custom-form-row">
                            <div class="col-sm-3">
                                <p class="feature_title form-label-title">
                                    Category
                                </p>
                            </div>
                            <div class="col-sm-9">
                                <div class="form-group">
                                    <select class="form-control custom-form-control" name="category_id" id="category_id" required="true">
                                        <option value="" selected="true" disabled="true">Select</option>
                                        <?php 
                                        if(count($category) >0) {                     
                                            foreach ($category as $one) {
                                            ?>
                                             <option value="<?php echo $one->id?>" <?php echo isset($data)? ( $data[0]->category_id == $one->id)?'selected="selected"':'':''; ?> ><?php echo $one->name; ?></option>
                                            <?php
                                            }
                                        }
                                        ?>                                        
                                    </select>
                                </div>
                            </div>
                        </div>
                    </li>

                     <li class="list-group-item border-none">
                        <div class="row custom-form-row">
                            <div class="col-sm-3">
                                <p class="feature_title form-label-title">
                                    Size
                                </p>
                            </div>
                            <div class="col-sm-9">
                                <div class="form-group">    
                                    <input type="text" class="form-control custom-form-control" name="size" id="size" value="<?php echo isset($data)?$data[0]->size:''; ?>" placeholder="size" >
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="list-group-item border-none">
                        <div class="row custom-form-row">
                            <div class="col-sm-3">
                                <p class="feature_title form-label-title">
                                    Description
                                </p>
                            </div>
                            <div class="col-sm-9">
                                <div class="form-group">
                                    <textarea class="form-control custom-form-control" rows="3" name="description" id="description" placeholder="Description" required="true"><?php echo isset($data)?$data[0]->description:''; ?></textarea>
                                </div>
                            </div>
                        </div>
                    </li>
                     <li class="list-group-item border-none">
                        <div class="row custom-form-row">
                            <div class="col-sm-3">
                                <p class="feature_title form-label-title">
                                    Images 
                                </p>
                            </div>
                            <div class="col-sm-9" id="images_block">                        
                                <div class="form-group">                            
                                    <input type="file" class="form-control custom-form-control" name="userFiles[]" placeholder="Upload files" accept=".jpg,.png,.gif,.jpeg,.PNG,.JPG,.JPEG,.GIF" multiple <?php echo isset($data)? '': 'required'; ?> >
                                </div>                                 
                            </div>
                        </div>
                    </li>
                </ul>
            </div>

            
            <div class="row">
                <div class="col-sm-12 text-center">
                    <button type="submit" class="btn custom-btn custom_btn  custom-save-btn" href="#">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>
<script type="text/javascript">
    $('form[name="add_product"]').validate({
         ignore: [],
   rules: {
          'name':{required: true},
          'category_id':{required: true},
          'description':{required: true}
    },
    errorPlacement: function(error, element){
      // if(element.attr('name') == "amenities[]"){
      //   error.insertAfter("#amenitieserrorsplace");      
      // }else{
        error.insertAfter(element);
      // }
    },
    // submitHandler: function(form) {  
    //     return true;      
    // } 
});
</script>