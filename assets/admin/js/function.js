$(document).ready(function(){
    //No empty space in textboxes
	$("input").on("keypress", function(e) {
    if (e.which === 32 && !this.value.length)
        e.preventDefault();
  });

  $("textarea").on("keydown", function (e) {
    var c = $("textarea").val().length;
    if(c == 0)
        return e.which !== 32;
  });

    //Jquery Validation for student add and edit form
    $("#student_form").validate({
        rules:{
            first_name:{required:true},
            last_name:{required:true},
            date_of_birth:{required:true},
            email_id:{required:true,email: true},
            contact_no:{required:true},
            address:{required:true},
            city:{required:true},
            state:{required:true},
            country:{required:true},
            guardian_first_name:{required:true},
            guardian_last_name:{required:true},
            guardian_email:{required:true},
            guardian_contact_no:{required:true},
            guardian_address:{required:true},
            guardian_city:{required:true},
            guardian_state:{required:true},
            guardian_country:{required:true},
            relationship_id:{required:true},
            admission_number:{required:true},
            class_id:{required:true},
            session_id:{required:true}
        },
        messages: {
            "first_name": {
                required: "Please Enter First Name"
            },
            "last_name": {
                required: "Please Enter Last Name"
            },
            "date_of_birth": {
                required: "Please Provide Date of Birth"
            },
            "email_id":{
                required:"Enter valid Email Address"
            },
            "contact_no":{
                required:"Enter Valid Mobile Number"
            },
            "address":{
                required:"Please Enter Address Detail"
            },
            "city":{
                required:"Please Enter City Name"
            },
            "state":{
                required:"Please Enter State Name"
            },
            "country":{
                required:"Please Enter Country Name"
            },
            "guardian_first_name":{
                required:"Please Enter Guardian's First Name"
            },
            "guardian_last_name":{
                required:"Please Enter Guardian's Last Name"
            },
            "guardian_email":{
                required:"Enter valid Email Address"
            },
            "guardian_contact_no":{
                required:"Enter Valid Mobile Number"
            },
            "guardian_address":{
                required:"Please Enter Guardian's Address Detail"
            },
            "guardian_city":{
                required:"Please Enter Guardian's City Name"
            },
            "guardian_state":{
                required:"Please Enter Guardian's State Name"
            },
            "guardian_country":{
                required:"Please Enter Guardian's Country Name"
            },
            "relationship_id":{
                required:"Please Select Relationship"
            },
            "admission_number":{
                required:"Please Enter Admission Number"
            },
            "class_id":{
                required:"Please Select Current Class"
            },
            "session_id":{
                required:"Please Select Current Session"
            }
        },  
    });
    //Jquery Validation Ends

    //Validation for upload documents.
    //$('input[id="exampleInputFile"]').change(function () {
    $( document ).on('change','input.exampleInputFile',function(){
        var ext = this.value.match(/\.(.+)$/)[1];
        switch (ext) {
              case 'jpg':
              case 'JPG':
              case 'jpeg':
              case 'JPEG':
              case 'png':
              case 'PNG':
              case 'pdf':
              case 'PDF':
              case 'doc':
              case 'DOC':
              case 'docx':
              case 'DOCX':
                  $("#error_msg_file").html("");
                  break;
              default:
                  $("#error_msg_file").html("Only JPG,PNG,PDF,DOC files are allowed")
                  this.value = '';
          }
    });

    //Validation for Image Upload
    $('input[id="photograph"]').change(function () {
      var ext = this.value.match(/\.(.+)$/)[1];
      switch (ext) {
            case 'jpg':
            case 'JPG':
            case 'jpeg':
            case 'JPEG':
            case 'png':
            case 'PNG':
                $("#error-photo").html("");
                break;
            default:
                $("#error-photo").html("Only JPG,PNG types are allowed")
                this.value = '';
      }
    });

    //Allowed Only Numbers in textbox
    $('.numbers_only').keydown(function(event) {
        // Allow special chars + arrows 
        if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 
            || event.keyCode == 27 || event.keyCode == 13 
            || (event.keyCode == 65 && event.ctrlKey === true) 
            || (event.keyCode >= 35 && event.keyCode <= 39)){
                return;
        }else {
            // If it's not a number stop the keypress
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                event.preventDefault(); 
            }   
        }
    });
    
    //On Close Button not shown validation message
    $(".btn-gray").click(function(){
        $(".error").hide();
        $(".error_block_class").hide();
        $(".error_block_session").hide();
        $(".error_block_amount").hide();
        $(".error_block_extra").hide();
        $(".error_block_feetype").hide();
    }); 

});
